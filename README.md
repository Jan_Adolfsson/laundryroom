# README #

This API is built in C# using Visual Studio 2019 and targeting .netcoreapp3.1. 

To build the project you need to install Visual Studio 2019. Download it here https://visualstudio.microsoft.com/downloads/


## Project Structure ##

Due to the small size of the project and the encouragement to not spend to much time on this I decided to put everything except tests in the same project.

For a bigger project I'd separated different layers or concerns in different projects.



### Build & Run ###

1. Start by cloning the repo:  
```git clone https://Jan_Adolfsson@bitbucket.org/Jan_Adolfsson/laundryroom.git```  

2. You can build and run this either from Visual Studio or from the command line.

    **Visual Studio**  
From Visual Studio open the Solution found at laundryroom/LaundryRoom.sln and start it. To be able to start it, you might have to set LaundryRoom.Api-project as the startup project.

    **Command line**  
Open a command prompt and navigate to laundryroom/LaundryRoom.Api. Type:  
```dotnet run```
3. Browse to ```https://localhost:44379/api/bookings``` to get all existing bookings.


Use a tool like Postman or Isomnia for testing the API. 


## Run in container ##

In Powershell or other console:  
1. Navigate to folder `laundryroom` (where the Dockerfile is located)  
2. Type:  
    ```docker build -t laundryroom .```  
3. Type:  
    ```docker run -it --rm -p 5000:80 --name laundryroom laundryroom```  
4. Browse to http://localhost:5000/api/bookings to get existing bookings.  


## API ##

The api has following endpoints:  

GET  
api/bookings  
*Get all existing bookings*

GET  
api/bookings/{id}  
*Get booking with given id*

POST  
api/bookings  
*Add a booking, requires body as JSON*

```
{
	"laundryRoomId":0,
	"householdId": 1,
	"noOfHours":1,
	"dateTime": "2020-03-31T10:00:00"
}
```

The time passed to bookings will be rounded down to nearest hour e.g. 10:45:00 will be rounded to 10:00:00.



DELETE  
api/bookings/{id}  
*Deletes(Cancels) booking with given id*
