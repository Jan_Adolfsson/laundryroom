﻿using System;
using System.Collections.Generic;
using LaundryRoom.Api.Dtos;
using LaundryRoom.Api.Interfaces.Services;
using LaundryRoom.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace LaundryRoom.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingService _bookingService;

        public BookingsController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        // GET: api/Bookings/{id}
        [HttpGet("{id}")]
        public ActionResult<Booking> Get(int id)
        {
            if (id < 0)
                return BadRequest("Invalid id.");

            var booking = _bookingService.Get(id);
            return Ok(booking);
        }

        // GET: api/Bookings
        [HttpGet]
        public ActionResult<IEnumerable<Booking>> Get()
        {
            var bookings = _bookingService.GetBookings();
            return Ok(bookings);
        }

        [HttpPost]
        public ActionResult Post([FromBody] BookingRequest bookingRequest)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid input.");

            var bookingModel = MapToModel(bookingRequest);

            if(!_bookingService.Book(bookingModel))
            {
                return BadRequest("Booking failed.");
            }

            return Ok();
        }

        private Booking MapToModel(BookingRequest request)
        {
            var model = new Booking
            {
                DateTime = request.DateTime.Date.AddHours(request.DateTime.Hour),
                HouseholdId = request.HouseholdId,
                LaundryRoomId = request.LaundryRoomId,
                NoOfHours = request.NoOfHours
            };

            return model;
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if(id < 0)
            {
                return BadRequest("Invalid id.");
            }

            if(!_bookingService.CancelBooking(id))
            {
                return BadRequest("Cancellation failed.");
            }

            return NoContent();
        }
    }
}
