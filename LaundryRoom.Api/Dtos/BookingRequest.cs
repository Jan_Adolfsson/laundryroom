﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LaundryRoom.Api.Dtos
{
    public class BookingRequest
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int HouseholdId { get; set; }
        
        [Required]
        [Range(0, int.MaxValue)]
        public int LaundryRoomId { get; set; }
        
        [Required]
        public DateTime DateTime { get; set; }
        
        [Required]
        [Range(1, 24)]
        public int NoOfHours { get; set; }
    }
}
