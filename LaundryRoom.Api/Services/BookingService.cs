﻿using LaundryRoom.Api.Interfaces.Repositories;
using LaundryRoom.Api.Interfaces.Services;
using LaundryRoom.Api.Models;
using System.Collections.Generic;

namespace LaundryRoom.Api.Services
{
    // This class should probably catch all errors and
    // return a response object for the controller to more or
    // less return to the client.
    public class BookingService : IBookingService
    {
        private readonly IBookingRepository _repository;
        private readonly BookingValidator _validator;

        public BookingService(IBookingRepository repository)
        {
            _repository = repository;
            _validator = new BookingValidator(repository);
        }

        public bool Book(Booking booking)
        {
            if (!_validator.Validate(booking))
                return false;

            _repository.Add(booking);

            return true;
        }

        public bool CancelBooking(int bookingId)
        {
            if (!_repository.Exists(bookingId))
                return false;

            _repository.Delete(bookingId);

            return true;
        }
        
        public Booking Get(int bookingId)
        {
            var booking = _repository.Get(bookingId);
            return booking;
        }
        public IEnumerable<Booking> GetBookings()
        {
            return _repository.GetAll();
        }
    }
}
