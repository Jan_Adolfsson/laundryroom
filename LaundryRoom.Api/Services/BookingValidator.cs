﻿using LaundryRoom.Api.Interfaces.Repositories;
using LaundryRoom.Api.Interfaces.Services;
using LaundryRoom.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LaundryRoom.Api.Services
{
    public class BookingValidator : IBookingValidator
    {

        // Could be fetched from database through various repositories. Those
        // repositories would be injectected together with the existing IBookingRepository.
        private readonly TimeSpan _openingTime = new TimeSpan(7, 0, 0);
        private readonly TimeSpan _closingTime = new TimeSpan(22, 0, 0);
        private readonly IEnumerable<int> _householdIds = Enumerable.Range(0, 20);
        private readonly IEnumerable<int> _laundryRoomIds = Enumerable.Range(0, 2);
        private readonly IBookingRepository _repository;

        public BookingValidator(IBookingRepository repository)
        {
            _repository = repository;
        }

        // Should return a response-object containing not only wether or not
        // validation succeeded, but also a proper error message in case of failure.
        public bool Validate(Booking booking)
        {
            if (!LaundryRoomExists(booking.LaundryRoomId))
                return false;

            if (!HouseholdExists(booking.HouseholdId))
                return false;

            if (!SlotWithinBookableHours(booking.DateTime.TimeOfDay, booking.NoOfHours))
                return false;

            if (!SlotIsFree(booking.DateTime, booking.LaundryRoomId, booking.NoOfHours))
                return false;

            return true;
        }


        public bool SlotIsFree(DateTime dateTime, int laundryRoomId, int noOfHours)
        {
            var bookings = _repository
                .GetByDate(dateTime.Date)
                .Where(b => b.LaundryRoomId == laundryRoomId)
                .OrderBy(b => b.DateTime)
                .ToList();

            if(bookings.Count == 0)
            {
                return true;
            }

            if(AnyBookingCollide(bookings, dateTime, noOfHours))
            {
                return false;
            }

            return true;
        }

        private bool AnyBookingCollide(IEnumerable<Booking> bookings, DateTime dateTime, int noOfHours)
        {
            var requestedSlot = ToInterval(dateTime.TimeOfDay, noOfHours);
            var collision = bookings
                .Any(b => 
                    CheckCollision(
                        ToInterval(b.DateTime.TimeOfDay, b.NoOfHours), 
                        requestedSlot));
            return collision;
        }

        private (TimeSpan start, TimeSpan end) ToInterval(TimeSpan time, int noOfHours)
        {
            return (time, time.Add(TimeSpan.FromHours(noOfHours)));
        }

        private bool CheckCollision((TimeSpan start, TimeSpan end) left, (TimeSpan start, TimeSpan end) right)
        {
            if(right.end <= left.start ||
                right.start >= left.end)
            {
                return false;
            }

            return true;
        }

        public bool SlotWithinBookableHours(TimeSpan time, int noOfHours)
        {
            var slotStart = time;
            var slotEnd = slotStart.Add(TimeSpan.FromHours(noOfHours));

            if (slotStart < _openingTime || slotStart >= _closingTime)
                return false;

            if (slotEnd > _closingTime)
                return false;

            return true;
        }

        public bool HouseholdExists(int householdId)
        {
            return _householdIds.Contains(householdId);
        }

        public bool LaundryRoomExists(int laundryRoomId)
        {
            return _laundryRoomIds.Contains(laundryRoomId);
        }
    }
}
