﻿using LaundryRoom.Api.Models;
using System;
using System.Collections.Generic;

namespace LaundryRoom.Api.Interfaces.Repositories
{
	public interface IBookingRepository
	{
		int Add(Booking booking);
		void Delete(int id);
		bool Exists(int id);
		IEnumerable<Booking> GetAll();
		Booking Get(int id);
		IEnumerable<Booking> GetByDate(DateTime date);
	}
}
