﻿using LaundryRoom.Api.Models;
using System.Collections.Generic;

namespace LaundryRoom.Api.Interfaces.Services
{
    public interface IBookingService
    {
        bool Book(Booking booking);
        bool CancelBooking(int bookingId);
        Booking Get(int bookingId);
        IEnumerable<Booking> GetBookings();
    }
}
