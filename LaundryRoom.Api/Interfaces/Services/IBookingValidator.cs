﻿using LaundryRoom.Api.Models;
using System;

namespace LaundryRoom.Api.Interfaces.Services
{
    public interface IBookingValidator
    {
        bool HouseholdExists(int householdId);
        bool LaundryRoomExists(int laundryRoomId);
        bool SlotIsFree(DateTime dateTime, int laundryRoomId, int noOfHours);
        bool SlotWithinBookableHours(TimeSpan time, int noOfHours);
        bool Validate(Booking booking);
    }
}