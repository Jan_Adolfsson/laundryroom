﻿using System;

namespace LaundryRoom.Api.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public int HouseholdId { get; set; }
        public int LaundryRoomId { get; set; }
        public DateTime DateTime { get; set; }
        public int NoOfHours { get; set; }
    }
}
