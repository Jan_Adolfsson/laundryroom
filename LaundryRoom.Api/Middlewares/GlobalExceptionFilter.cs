﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace LaundryRoom.Api.Middlewares
{
    // Catches all exceptions thrown within controller. This way
    // we don't have to write a lot of try-catch-statements inside
    // controller.
    public class GlobalExceptionFilter : ExceptionFilterAttribute
    {
        public GlobalExceptionFilter()
        {
        }

        // Catches exceptions for logging etc.
        public override void OnException(ExceptionContext context)
        {
            base.OnException(context);
        }
    }
}
