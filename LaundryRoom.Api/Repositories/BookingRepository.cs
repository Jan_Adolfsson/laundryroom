﻿using LaundryRoom.Api.Interfaces.Repositories;
using LaundryRoom.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LaundryRoom.Api.Repositories
{
    public class BookingRepository : IBookingRepository
    {
        List<Booking> _bookings = new List<Booking>(
            //new[]
            //{
            //    new Booking
            //    {
            //        DateTime = new DateTime(2020, 3, 31, 8, 0, 0),
            //        HouseholdId = 0,
            //        Id = 0,
            //        LaundryRoomId = 0,
            //        NoOfHours = 2
            //    },
            //    new Booking
            //    {
            //        DateTime = new DateTime(2020, 3, 31, 12, 0, 0),
            //        HouseholdId = 10,
            //        Id = 1,
            //        LaundryRoomId = 0,
            //        NoOfHours = 2
            //    }
            //}
        );
        public int Add(Booking booking)
        {
            var maxId = _bookings.Count > 0 ? _bookings.Max(b => b.Id) + 1 : 0;

            booking.Id = maxId;
            _bookings.Add(booking);

            return booking.Id;
        }

        public void Delete(int bookingId)
        {
            _bookings.RemoveAll(b => b.Id == bookingId);
        }

        public bool Exists(int bookingId)
        {
            return _bookings.Exists(b => b.Id == bookingId);
        }

        public Booking Get(int id)
        {
            var booking = _bookings.Single<Booking>(b => b.Id == id);
            return booking;
        }
        public IEnumerable<Booking> GetAll()
        {
            return _bookings;
        }

        public IEnumerable<Booking> GetByDate(DateTime date)
        {
            return _bookings.Where(b => b.DateTime.Date == date.Date);
        }
    }
}
