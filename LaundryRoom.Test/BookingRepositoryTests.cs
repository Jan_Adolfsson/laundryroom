using LaundryRoom.Api.Models;
using LaundryRoom.Api.Repositories;
using Shouldly;
using System;
using System.Linq;
using Xunit;

namespace LaundryRoomBooking.Test
{
	public class BookingRepositoryTests
	{
		public class Add
		{
			[Fact]
			public void Should_IncreaseItemCount()
			{
				// Arrange
				var repo = new BookingRepository();
				var noOfBookingsBefore = repo.GetAll().Count();

				// Act
				repo.Add(new Booking());

				// Assert
				var bookings = repo.GetAll();
				bookings.Count().ShouldBe(noOfBookingsBefore + 1);
			}

			[Fact]
			public void Should_ReturnIdOfAddedItem()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking());
				var id = repo.Add(new Booking());
				var expectedId = id + 1;

				// Assert id doesn't exist before
				var bookings = repo.GetAll();
				bookings.ShouldNotContain(b => b.Id == expectedId);

				// Act
				var newId = repo.Add(new Booking());

				// Assert
				newId.ShouldBe(expectedId);
			}

			[Fact]
			public void Should_AddItemWithAllItsPropertiesExceptId()
			{
				// Arrange
				var repo = new BookingRepository();

				// Act
				var newId = repo.Add(
					new Booking
					{
						DateTime = new DateTime(2020, 3, 27),
						HouseholdId = 1,
						Id = 42,
						LaundryRoomId = 2,
						NoOfHours = 4
					});

				// Assert
				newId.ShouldBe(0);
				var booking = repo.GetAll().Single();

				booking.DateTime.ShouldBe(new DateTime(2020, 3, 27));
				booking.HouseholdId.ShouldBe(1);
				booking.Id.ShouldBe(newId);
				booking.LaundryRoomId.ShouldBe(2);
				booking.NoOfHours.ShouldBe(4);
			}
		}

		public class Delete
		{
			[Fact]
			public void Should_RemoveOneItem()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking());
				var id = repo.Add(new Booking());

				// Act
				repo.Delete(id);
				var bookings = repo.GetAll();

				// Assert
				bookings.Count().ShouldBe(1);
			}

			[Fact]
			public void Should_RemoveItem()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking());
				var id = repo.Add(new Booking());

				// Assert it exists before delete
				var bookings = repo.GetAll();
				bookings.ShouldContain(b => b.Id == id);

				// Act
				repo.Delete(id);

				// Assert it doesn't exist after
				bookings = repo.GetAll();
				bookings.ShouldNotContain(b => b.Id == id);
			}
		}

		public class Exists
		{
			[Fact]
			public void Should_ReturnTrue_When_ItemExists()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking());
				repo.Add(new Booking());
				var expectedId = 2;

				// Assert it doesn't exist before add
				var exists = repo.Exists(expectedId);
				exists.ShouldBeFalse();

				// Act
				repo.Add(new Booking());
				exists = repo.Exists(expectedId);

				// Assert it exist after
				exists.ShouldBeTrue();
			}
		}

		public class GetAll
		{
			[Fact]
			public void Should_ReturnAllItems()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking());
				repo.Add(new Booking());

				// Act
				var bookings = repo.GetAll();

				// Assert
				bookings.Count().ShouldBe(2);
			}
		}

		public class GetByDate
		{
			[Fact]
			public void Should_OnlyReturnItemForGivenDate()
			{
				// Arrange
				var repo = new BookingRepository();
				repo.Add(new Booking { DateTime = new DateTime(2020, 3, 27) });
				repo.Add(new Booking { DateTime = new DateTime(2020, 3, 28) });
				repo.Add(new Booking { DateTime = new DateTime(2020, 3, 29) });
				var expectedDate = new DateTime(2020, 3, 28);

				// Act
				var bookings = repo.GetByDate(expectedDate);

				// Assert
				bookings.Count().ShouldBe(1);
				bookings.Single().DateTime.ShouldBe(expectedDate);
			}
		}
	}
}
