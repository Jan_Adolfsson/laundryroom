﻿using LaundryRoom.Api.Interfaces.Repositories;
using LaundryRoom.Api.Models;
using LaundryRoom.Api.Services;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using Xunit;

namespace LaundryRoom.Test
{
    public class BookingValidatorTests
    {
        public class HouseholdExists
        {
            [Fact]
            public void Should_ReturnTrue_When_Exists()
            {
                // Arrange
                var validator = new BookingValidator(null);

                // Act & Assert
                // There exists 20 household with ids 0 - 19
                validator.HouseholdExists(19).ShouldBeTrue();
                validator.HouseholdExists(0).ShouldBeTrue();

                validator.HouseholdExists(-1).ShouldBeFalse();
                validator.HouseholdExists(20).ShouldBeFalse();
            }
        }

        public class LaundryRoomExists
        {
            [Fact]
            public void Should_ReturnTrue_When_Exists()
            {
                // Arrange
                var validator = new BookingValidator(null);

                // Act & Assert
                // There exists 2 laundry rooms with ids 0 & 1
                validator.LaundryRoomExists(0).ShouldBeTrue();
                validator.LaundryRoomExists(1).ShouldBeTrue();

                validator.LaundryRoomExists(-1).ShouldBeFalse();
                validator.LaundryRoomExists(2).ShouldBeFalse();
            }
        }

        public class WithinBookableHours
        {
            [Fact]
            public void Should_ReturnTrue_When_Within()
            {
                // Arrange
                var validator = new BookingValidator(null);

                // Act & Assert
                // Bookable hours is between 7 - 22.
                validator.SlotWithinBookableHours(TimeSpan.FromHours(7), 15).ShouldBeTrue();

                // Too early
                validator.SlotWithinBookableHours(TimeSpan.FromHours(6), 1).ShouldBeFalse();
                validator.SlotWithinBookableHours(TimeSpan.FromHours(6), 2).ShouldBeFalse();

                // Too late
                validator.SlotWithinBookableHours(TimeSpan.FromHours(22), 1).ShouldBeFalse();
                validator.SlotWithinBookableHours(TimeSpan.FromHours(21), 2).ShouldBeFalse();
            }
        }


        public class SlotIsFree
        {
            public static IEnumerable<object[]> TestDataForFreeSlots 
            {
                get {
                    yield return new object[] { new DateTime(2020, 3, 31, 9, 0, 0), 1 };
                    yield return new object[] { new DateTime(2020, 3, 31, 12, 0, 0), 2 };
                    yield return new object[] { new DateTime(2020, 3, 31, 16, 0, 0), 1 };
                }
            }
            [Theory, MemberData(nameof(TestDataForFreeSlots))]
            public void Should_ReturnTrue_When_SlotIsFree(DateTime slot, int noOfHours)
            {
                // Arrange
                var repo = new Mock<IBookingRepository>();
                repo
                    .Setup(
                        r =>
                            r.GetByDate(new DateTime(2020, 3, 31, 0, 0, 0)))
                    .Returns(
                        new Booking[]
                        {
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 10, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 1
                            },
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 11, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 1
                            },
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 14, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 2
                            },
                        });

                var validator = new BookingValidator(repo.Object);

                // Act & Assert
                validator.SlotIsFree(slot, 0, noOfHours).ShouldBeTrue();
            }

            public static IEnumerable<object[]> TestDataForOccupiedSlots
            {
                get
                {
                    yield return new object[] { new DateTime(2020, 3, 31, 9, 0, 0), 2 };
                    yield return new object[] { new DateTime(2020, 3, 31, 10, 0, 0), 1 };
                    yield return new object[] { new DateTime(2020, 3, 31, 11, 0, 0), 3 };
                    yield return new object[] { new DateTime(2020, 3, 31, 15, 0, 0), 1 };
                    yield return new object[] { new DateTime(2020, 3, 31, 16, 0, 0), 1 };
                }
            }

            [Theory, MemberData(nameof(TestDataForOccupiedSlots))]
            public void Should_ReturnFalse_When_SlotIsOccupied(DateTime slot, int noOfHours)
            {
                // Arrange
                var repo = new Mock<IBookingRepository>();
                repo
                    .Setup(
                        r =>
                            r.GetByDate(new DateTime(2020, 3, 31, 0, 0, 0)))
                    .Returns(
                        new Booking[]
                        {
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 10, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 1
                            },
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 12, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 1
                            },
                            new Booking
                            {
                                DateTime = new DateTime(2020, 3, 31, 14, 0, 0),
                                LaundryRoomId = 0,
                                NoOfHours = 3
                            },
                        });

                var validator = new BookingValidator(repo.Object);

                // Act & Assert
                validator.SlotIsFree(slot, 0, noOfHours).ShouldBeFalse();
            }
        }
    }
}
